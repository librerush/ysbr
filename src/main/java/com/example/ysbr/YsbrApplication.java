package com.example.ysbr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YsbrApplication {

    public static void main(String[] args) {
        SpringApplication.run(YsbrApplication.class, args);
    }

}
